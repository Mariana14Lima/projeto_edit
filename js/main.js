$(document).ready(function(){

    //menu toggle
    var burgerTag = $('.burger');
    var navMobile = $('.nav-mobile');
    burgerTag.on('click',function(){
        navMobile.toggleClass('nav-visible');
        
        if(navMobile.hasClass('nav-visible')){
            burgerTag.html("<img src='img/close.svg'>");
        } else{
            burgerTag.html("<img src='img/burger.svg'>");
        }
    })

    //blog
    var mainImage = $('.main-image');
    var mainImageSrc = mainImage.attr('src');
    var imageTag = $('.blog .wrapper div ul li img');
    var title = $('.title');
    var titleMessage = title.html();
    
    imageTag.on('click', function(){
        var imageTagSrc = $(this).attr('src');
        mainImage.attr('src', imageTagSrc);
        $(this).attr('src', mainImageSrc);
        mainImageSrc = mainImage.attr('src');
  
        var text = $(this).attr('data-text');
        title.html(text);
        $(this).attr('data-text',titleMessage);
        titleMessage = text;
    });

    //slider
    $('.intro-slider').slick({
        dots: true,
        arrows: false,
        autoplay:true,
        autoplaySpeed: 3000
    });

    $('.slider').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 512,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });


    

    //fadeIn
    $(document).on('scroll', function(){
        var pageTop = $(document).scrollTop();
        var pageBottom = pageTop + $(window).height();
        var cards = $('.rooms .flex-container .card');

        if($(cards).position().top < pageBottom - 300){
            $(cards).addClass('visible');
        }

    })

    //smooth scroll
    $(document).on('click', 'a[href^="#"]', function (event) {
        event.preventDefault();
    
        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 500);
    });


    //filters-toggle
    var filter = $('.filter');
    var filtersList = $('.filters');
    
    $(filter).on('click', function(){
        $(this).children("ul").fadeToggle(600);
    })

    $(filtersList).on('click', function(){
        $('.all-products').hide();
        var filters = $(this).attr('data-type');
        $(filtersList).removeClass('selected');
        $(this).addClass('selected');
        $(filters).show();
    })
    
   //basket items
    var basketTag = $('.icon');
    var currentItem = $('.items');
    var itemAlert = $('.item-alert');
    var initialItem = 0;

    basketTag.on('click', function(){
        itemAlert.addClass('item-alert-visible')
        setTimeout(()=>{
            itemAlert.removeClass('item-alert-visible')
        },3000)
        initialItem = initialItem + 1;
        currentItem.text("(" + initialItem + ")");
        
    })


})